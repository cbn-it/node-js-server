const GetConfigs = require('./GetConfigs.js');
const {stringify} = require('./stringify.js');
const fs = require("fs");
const path = require("path");
const dayjs = require("dayjs");

/**
 * @abstract
 */
class AbstractIndex extends GetConfigs {

    indexFile = 'index.html';
    requiredLogin = false;


    _redirectWithContinue(urlPath) {
        let url = new URL(this.req.protocol + '://' + this.req.get('host') + urlPath);
        url.searchParams.append("continue", this.req.url);
        this.res.redirect(url);
        return url.toString();
    }

    async execute() {
        let user = await this.getUser();
        if (user === null) {
            this._redirectWithContinue('/login');
            return
        }
        let account = await this.getAccount();
        if (account === null || account === undefined) {
            this._redirectWithContinue('/login/no-account/' + user.email);
            return
        }
        if (account.blockedAccess === true) {
            this._redirectWithContinue('/login/account-ban/' + user.email);
            return;
        }
        if (account.blockedAccessCompany === true) {
            this._redirectWithContinue('/login/company-ban/' + account._companyName);
            return
        }
        if ((account.blockedAccessCompanyDate || "") !== "" && dayjs().format("YYYY-MM-DD") >= account.blockedAccessCompanyDate) {
            this._redirectWithContinue('/login/company-ban/' + account._companyName);
            return
        }
        let index = fs.readFileSync(path.join(global.projectRoot, "web", this.indexFile), 'utf8');
        let data = await this._getData();
        this.res.send(index.replace('"data_to_replace"', stringify(data)));
    }

    async _getData() {
        let companies = await this.getAllUserCompanies();
        companies.sort((a, b) => a.companyName.localeCompare(b.companyName));
        let _companyId = await this._getDefaultCompany(companies);
        let forms = await this._getForms('server/configs', 'form', _companyId, true);
        let columns = await this._getForms('server/columns', 'column', _companyId, true);
        let reports = await this._getReports('report', _companyId, true);
        let user = await this.getUser();
        let response = {
            _appId: process.env.GOOGLE_CLOUD_PROJECT,
            NODE_ENV: process.env.NODE_ENV,
            _configs: forms,
            _columns: columns,
            _reports: reports,
            _companies: companies,
            _selectedCompany: _companyId,
            _user: user
        };
        return Object.assign(response, await this.getData(_companyId,response));
    }

    /**
     * @abstract
     * @param _companyId
     * @returns {Object}
     */
    getData(_companyId) {
        return {};
    }

    async _getReports(collection, _companyId) {
        let allReports = await this.runQuery('', collection, []);
        if (_companyId !== 'default') {
            let reports = await this.runQuery(_companyId, collection, []);
            allReports = [...allReports, ...reports];
        }
        return allReports;
    }

    async _getDefaultCompany(companies) {
        let account = await this.getAccount();
        let companyIds = companies.map(value => value._id);

        let defaultOptions = account ? await this.getDocument('', 'defaultOptions', account.accountEmail) : null;

        let companyIdDefaultOptions = defaultOptions ? defaultOptions.defaultCompany : null;
        let companyIdParam = this.req.param['_companyId'];

        if (!companyIdParam || !companyIds.includes(companyIdParam)) {
            companyIdParam = null;
        }
        if (!companyIdDefaultOptions || !companyIds.includes(companyIdDefaultOptions)) {
            companyIdDefaultOptions = null;
        }

        let defaultCompany = companyIdParam || companyIdDefaultOptions || companyIds[0];
        if (defaultCompany !== '' && defaultCompany !== 'default' && (!defaultOptions || defaultCompany !== defaultOptions.defaultCompany)) {
            await this.updateDocument('', 'defaultOptions', account.accountEmail, {defaultCompany}, true);
        }
        return defaultCompany;
    }
}

module.exports = AbstractIndex;