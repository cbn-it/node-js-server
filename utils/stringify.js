const _WHITESPACE = new Array(512).fill(' ').join('');
const _format_date = function (n) {
    return n < 10 ? '0' + n : '' + n;
};
const stringify = (data, indent) => {
    indent = typeof indent === 'string' ? indent : '';
    let str = '';
    if (typeof data === 'boolean'
        || data === null
        || data === undefined
        || (
            typeof data === 'number'
            && (
                data === Infinity
                || data === -Infinity
                || isNaN(data) === true
            )
        )
    ) {

        if (data === null) {
            str = indent + 'null';
        } else if (data === undefined) {
            str = indent + 'undefined';
        } else if (data === false) {
            str = indent + 'false';
        } else if (data === true) {
            str = indent + 'true';
        } else if (data === Infinity) {
            str = indent + 'Infinity';
        } else if (data === -Infinity) {
            str = indent + '-Infinity';
        } else if (isNaN(data) === true) {
            str = indent + 'NaN';
        }

    } else if (typeof data === 'number') {

        str = indent + data.toString();

    } else if (typeof data === 'string') {

        str = indent + JSON.stringify(data);

    } else if (typeof data === 'function') {

        let body = data.toString().split('\n');
        let offset = 0;

        let first = body.find(ch => ch.startsWith('\t')) || null;

        if (first !== null) {

            let check = /(^\t+)/g.exec(first);
            if (check !== null) {
                offset = Math.max(0, check[0].length - indent.length);
            }

        }


        for (let b = 0, bl = body.length; b < bl; b++) {

            let line = body[b];
            if (line.startsWith('\t')) {
                str += indent + line.substr(offset);
            } else {
                str += indent + line;
            }

            str += '\n';

        }

    } else if (data instanceof Array) {
        //str += JSON.stringify(data);

        let is_primitive = data.find(val => val instanceof Object || typeof val === 'function') === undefined;

        let dimension = Math.sqrt(data.length);
        let is_matrix = dimension === Math.round(dimension);

        if (data.length === 0) {

            str = indent + '[]';

        } else if (is_primitive === true && is_matrix === true) {

            let max = 0;

            for (let d = 0, dl = data.length; d < dl; d++) {
                max = Math.max(max, (data[d]).toString().length);
            }


            str = indent;
            str += '[\n';

            for (let y = 0; y < dimension; y++) {

                str += '\t' + indent;

                for (let x = 0; x < dimension; x++) {

                    let tmp = stringify(data[x + y * dimension]);
                    if (tmp.length < max) {
                        str += _WHITESPACE.substr(0, max - tmp.length);
                    }

                    str += tmp;

                    if (x < dimension - 1) {
                        str += ', ';
                    }

                }

                if (y < dimension - 1) {
                    str += ',';
                }

                str += '\n';

            }

            str += indent + ']';

        } else if (is_primitive === true) {

            str = indent;
            str += '[';

            for (let d = 0, dl = data.length; d < dl; d++) {

                if (d === 0) {
                    str += ' ';
                }

                str += stringify(data[d]);

                if (d < dl - 1) {
                    str += ', ';
                } else {
                    str += ' ';
                }

            }

            str += ']';

        } else {

            str = indent;
            str += '[\n';

            for (let d = 0, dl = data.length; d < dl; d++) {

                str += stringify(data[d], '\t' + indent);

                if (d < dl - 1) {
                    str += ',';
                }

                str += '\n';

            }

            str += indent + ']';

        }

    } else if (data instanceof Date) {

        str = indent;

        str += '"';
        str += data.getUTCFullYear() + '-';
        str += _format_date(data.getUTCMonth() + 1) + '-';
        str += _format_date(data.getUTCDate()) + 'T';
        str += _format_date(data.getUTCHours()) + ':';
        str += _format_date(data.getUTCMinutes()) + ':';
        str += _format_date(data.getUTCSeconds()) + 'Z';
        str += '"';

    } else if (data instanceof Object) {

        let keys = Object.keys(data);
        if (keys.length === 0) {

            str = indent + '{}';

        } else {

            str = indent;
            str += '{\n';

            for (let k = 0, kl = keys.length; k < kl; k++) {

                let key = keys[k];

                str += '\t' + indent + '"' + key + '": ';
                str += stringify(data[key], '\t' + indent).trim();

                if (k < kl - 1) {
                    str += ',';
                }

                str += '\n';

            }

            str += indent + '}';

        }

    }


    return str;

};

module.exports = {stringify};